﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WpfCIBuildsTestApp;

namespace WpfCIBuildsTestAppUnitTests {
    [TestClass]
    public class AdderTests {
        [TestMethod]
        public void TestAdd() {
            Assert.AreEqual(3, Adder.Add(1, 2));
        }
    }
}
